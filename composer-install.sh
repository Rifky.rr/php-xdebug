#!/bin/bash
# need LF file
# check if plugin exsist
if test -f /usr/local/bin/install-php-extensions
then
    echo 'install Composer from Plugin'

    # install composer
    install-php-extensions @composer

    # clean up
    apt-get autoremove -y \
        && apt-get clean \
        && apt-get autoclean \
        && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
    exit
else
    echo 'install Composer from source'
    # remove composer.phar from current directory
    if test -f composer.phar
    then
        rm composer.phar
    fi

    # start install composer
    EXPECTED_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    ACTUAL_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

    if [ "$EXPECTED_CHECKSUM" != "$ACTUAL_CHECKSUM" ]
    then
        >&2 echo 'ERROR: Invalid installer checksum'
        rm composer-setup.php
        exit 1
    fi

    php composer-setup.php --quiet
    RESULT=$?

    # install composer to global
    if test -f composer.phar
    then
        mv composer.phar /usr/local/bin/composer

        echo 'composer installed to /usr/local/bin/composer'
        rm composer-setup.php
        exit $RESULT
    else
        echo 'ERROR: something failed'
        exit 1
    fi
fi