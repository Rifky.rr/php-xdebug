$whatToBuild = @(
    # "54",
    # "55",
    # "56",
    # "70",
    # "71",
    # "72",
    # "73",
    # "74",
    "80",
    "81",
    "82",
    "83",
    "84"
);

$typeToBuild = @(
    "apache",
    "fpm"
    # "cli"
);

$imageName = 'php';
$userName = 'chocin';
$tagDockerfile = '.Dockerfile';

[bool]$buildImage = 1;
[bool]$pushToHub = 1;
[bool]$cleanLocal = 1;

[bool]$debug = 0;
################################################################################
$phpSource = Get-Content .\source\versions.json | Out-String | ConvertFrom-Json;

foreach($version in $whatToBuild) {
    $version = $version.Trim();

    $dataSource = $phpSource.$version;
    $versionFolder = $dataSource.folder;
    $statusImage = $dataSource.type.ToLower();

    foreach($type in $typeToBuild) {
        $type = $type.Trim();

        $typeStr = $type;
        if($statusImage -ne 'stable') {
            $typeStr = "${statusImage}-${type}";
        }

        if ($debug) {
            echo "docker build . -f ${versionFolder}/${type}${tagDockerfile} -t ${userName}/${imageName}:${versionFolder}-${typeStr}-xdebug --no-cache";
        }
        else {
            
            if ($buildImage) {
                docker build . -f ${versionFolder}/${type}${tagDockerfile} -t ${userName}/${imageName}:${versionFolder}-${typeStr}-xdebug #--no-cache
            }
        
            if ($pushToHub) {
                docker push ${userName}/${imageName}:${versionFolder}-${typeStr}-xdebug
            }
        }
    }
}

if ($cleanLocal) {
    docker image prune -a -f
    docker builder prune -a -f
}