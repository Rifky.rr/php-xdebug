# php-xdebug

This is the Git repo for [chocin/php](https://hub.docker.com/r/chocin/php)\
Basend on [php - Docker Official Image](https://hub.docker.com/_/php)\
with updated base image, installed extension with [install-php-extensions](https://github.com/mlocati/docker-php-extension-installer) (php:5.5 up)\
sources.list on base image debian jessie, strech has been update to archive

checkout my other repo for docker compose / docker run collection on [GitLab](https://gitlab.com/docker.rifky.r3/web-server-docker)

Added Extension :
- mysql (php 5)
- mysqli
- mcrypt (except php 8.2)
- gd
- intl
- bcmath
- calendar
- ctype
- bz2
- zip
- pgsql
- pdo_mysql
- pdo_pgsql
- exif
- xdebug
- composer (installation file `/home/composer-install.sh`)