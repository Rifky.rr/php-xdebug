FROM php:7.2-cli-buster

COPY composer-install.sh /home/composer-install.sh
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN apt-get update && apt-get install \
        # debian-ports-archive-keyring \
        # debian-archive-keyring \
        ca-certificates \
        apt-utils \
        dos2unix -y  \
    && chmod +x /usr/local/bin/install-php-extensions \ 
    # && install-php-extensions @fix_letsencrypt \
    && update-ca-certificates \
    && apt-get update && apt-get upgrade -y  \
    && apt-get update && apt-get install --no-install-recommends -y  \
        firebird-dev \
        libtidy-dev \
        libsnmp-dev \
        curl \
        wget \
    && install-php-extensions \
        gd \
        xdebug \
        mysqli \
        # mysql \
        mcrypt \
        intl \
        bcmath \
        calendar \
        zip \
        sockets \
        bz2 \
        pgsql \
        pdo_mysql \
        pdo_pgsql \
        exif \
        mbstring \
    && chmod +x /home/composer-install.sh \
    && dos2unix /home/composer-install.sh \
    && /home/composer-install.sh \
    && apt-get autoremove -y \
    && apt-get clean \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

