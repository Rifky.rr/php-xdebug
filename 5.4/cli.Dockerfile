FROM php:5.4-cli
COPY debian_jessie_source.list /etc/apt/sources.list
COPY composer-install.sh /home/composer-install.sh

RUN apt-get update && apt-get install \
        debian-ports-archive-keyring \
        debian-archive-keyring \
        ca-certificates \
        apt-utils \
        dos2unix -y --force-yes \
    && update-ca-certificates \
    && apt-get update && apt-get upgrade -y --force-yes \
    && apt-get update && apt-get install --no-install-recommends -y --force-yes \
        firebird-dev \
        apt-utils \
        libtidy-dev \
        libsnmp-dev \
        libmcrypt-dev \
        libfreetype6-dev libjpeg62-turbo-dev libpng12-dev \
        libicu-dev \
        libbz2-dev \
        libpq-dev \
        curl \
        wget \
    && pecl install xdebug-2.4.1 && docker-php-ext-enable xdebug \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install \
        mysql \
        mysqli \
        mcrypt \
        gd \
        intl \
        bcmath \
        calendar \
        ctype \
        bz2 \
        zip \
        pgsql  \
        pdo_mysql \
        pdo_pgsql \
        exif \
        mbstring \
    && chmod +x /home/composer-install.sh \
    && dos2unix /home/composer-install.sh \
    && /home/composer-install.sh \
    && apt-get autoremove -y \
    && apt-get clean \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*