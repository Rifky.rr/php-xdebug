$whatToGenerate = @(
    "55",
    "56",
    "70",
    "71",
    "72",
    "73",
    "74",
    "80",
    "81",
    "82",
    "83",
    "84"
);

$typeToGenerate = @(
    "apache",
    "fpm",
    "cli"
);

##########################
$versions = Get-Content .\source\versions.json | Out-String | ConvertFrom-Json;
$main = Get-Content .\source\Dockerfile | Out-String;

for ($i = 0; $i -lt $whatToGenerate.Count; $i++) {
    $version = $whatToGenerate[$i];
    
    $folder = $versions.$version.folder
    if ((Test-Path -Path $folder)) {
        Remove-Item -Path $folder -Recurse -Force;
    }
    mkdir $folder;

    for ($j = 0; $j -lt $typeToGenerate.Count; $j++) {
        $type = $typeToGenerate[$j];

        $dockerFileName = "${type}.Dockerfile";
        $dockerFilePath = "${folder}\${dockerFileName}";

        New-Item -Path $dockerFilePath -ItemType File;

        $from = $versions.$version.$type;
        Add-Content -Path $dockerFilePath -Value "FROM ${from}";

        $repo = $versions.$version.repo;
        if ($repo -ne "") {
            Add-Content -Path $dockerFilePath -Value "COPY ${repo} /etc/apt/sources.list";
        }

        Add-Content -Path $dockerFilePath -Value "`nCOPY composer-install.sh /home/composer-install.sh";
        Add-Content -Path $dockerFilePath -Value "ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/";

        Add-Content -Path $dockerFilePath -Value "`n${main}";

        if ([int]$version -ge 56) {
            (Get-Content -Path $dockerFilePath) -replace "--force-yes", "" | Set-Content $dockerFilePath;
        }

        if ([int]$version -ge 70) {
            (Get-Content -Path $dockerFilePath) -replace " mysql ", " # mysql " | Set-Content $dockerFilePath;
        }

        if ([int]$version -ge 83) {
            (Get-Content -Path $dockerFilePath) -replace " mcrypt ", " # mcrypt " | Set-Content $dockerFilePath;
        }

        # disable xdebug for RC
        if ($versions.$version.type -ne "STABLE") {
            (Get-Content -Path $dockerFilePath) -replace "xdebug", "# xdebug" | Set-Content $dockerFilePath;
        }

        if ($repo -eq "") {
            (Get-Content -Path $dockerFilePath) -replace "debian-ports-archive-keyring", "# debian-ports-archive-keyring" | Set-Content $dockerFilePath;
            (Get-Content -Path $dockerFilePath) -replace "debian-archive-keyring", "# debian-archive-keyring" | Set-Content $dockerFilePath;
            (Get-Content -Path $dockerFilePath) -replace "&& install-php-extensions @fix_letsencrypt", "# && install-php-extensions @fix_letsencrypt" | Set-Content $dockerFilePath;
        }

        if ($type -eq "apache") {
            Add-Content -Path $dockerFilePath -Value "RUN a2enmod rewrite";
        }
    }
}